// package: language.service.v1.private
// file: v1/private/locale.proto

import * as v1_private_locale_pb from "../../v1/private/locale_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import {grpc} from "@improbable-eng/grpc-web";

type LocaleServiceGetLocale = {
  readonly methodName: string;
  readonly service: typeof LocaleService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_locale_pb.GetLocaleRequest;
  readonly responseType: typeof v1_private_locale_pb.GetLocaleResponse;
};

type LocaleServiceCreateLocale = {
  readonly methodName: string;
  readonly service: typeof LocaleService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_locale_pb.CreateLocaleRequest;
  readonly responseType: typeof v1_private_locale_pb.CreateLocaleResponse;
};

type LocaleServiceUpdateLocale = {
  readonly methodName: string;
  readonly service: typeof LocaleService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_locale_pb.UpdateLocaleRequest;
  readonly responseType: typeof v1_private_locale_pb.UpdateLocaleResponse;
};

type LocaleServiceDeleteLocale = {
  readonly methodName: string;
  readonly service: typeof LocaleService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_locale_pb.DeleteLocaleRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type LocaleServiceListLocales = {
  readonly methodName: string;
  readonly service: typeof LocaleService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_locale_pb.ListLocalesRequest;
  readonly responseType: typeof v1_private_locale_pb.ListLocalesResponse;
};

type LocaleServiceGetDefault = {
  readonly methodName: string;
  readonly service: typeof LocaleService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_locale_pb.GetDefaultRequest;
  readonly responseType: typeof v1_private_locale_pb.GetDefaultResponse;
};

export class LocaleService {
  static readonly serviceName: string;
  static readonly GetLocale: LocaleServiceGetLocale;
  static readonly CreateLocale: LocaleServiceCreateLocale;
  static readonly UpdateLocale: LocaleServiceUpdateLocale;
  static readonly DeleteLocale: LocaleServiceDeleteLocale;
  static readonly ListLocales: LocaleServiceListLocales;
  static readonly GetDefault: LocaleServiceGetDefault;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class LocaleServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  getLocale(
    requestMessage: v1_private_locale_pb.GetLocaleRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_private_locale_pb.GetLocaleResponse|null) => void
  ): UnaryResponse;
  getLocale(
    requestMessage: v1_private_locale_pb.GetLocaleRequest,
    callback: (error: ServiceError|null, responseMessage: v1_private_locale_pb.GetLocaleResponse|null) => void
  ): UnaryResponse;
  createLocale(
    requestMessage: v1_private_locale_pb.CreateLocaleRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_private_locale_pb.CreateLocaleResponse|null) => void
  ): UnaryResponse;
  createLocale(
    requestMessage: v1_private_locale_pb.CreateLocaleRequest,
    callback: (error: ServiceError|null, responseMessage: v1_private_locale_pb.CreateLocaleResponse|null) => void
  ): UnaryResponse;
  updateLocale(
    requestMessage: v1_private_locale_pb.UpdateLocaleRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_private_locale_pb.UpdateLocaleResponse|null) => void
  ): UnaryResponse;
  updateLocale(
    requestMessage: v1_private_locale_pb.UpdateLocaleRequest,
    callback: (error: ServiceError|null, responseMessage: v1_private_locale_pb.UpdateLocaleResponse|null) => void
  ): UnaryResponse;
  deleteLocale(
    requestMessage: v1_private_locale_pb.DeleteLocaleRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  deleteLocale(
    requestMessage: v1_private_locale_pb.DeleteLocaleRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  listLocales(
    requestMessage: v1_private_locale_pb.ListLocalesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_private_locale_pb.ListLocalesResponse|null) => void
  ): UnaryResponse;
  listLocales(
    requestMessage: v1_private_locale_pb.ListLocalesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_private_locale_pb.ListLocalesResponse|null) => void
  ): UnaryResponse;
  getDefault(
    requestMessage: v1_private_locale_pb.GetDefaultRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_private_locale_pb.GetDefaultResponse|null) => void
  ): UnaryResponse;
  getDefault(
    requestMessage: v1_private_locale_pb.GetDefaultRequest,
    callback: (error: ServiceError|null, responseMessage: v1_private_locale_pb.GetDefaultResponse|null) => void
  ): UnaryResponse;
}

