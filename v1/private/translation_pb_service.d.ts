// package: language.service.v1.private
// file: v1/private/translation.proto

import * as v1_private_translation_pb from "../../v1/private/translation_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import {grpc} from "@improbable-eng/grpc-web";

type TranslationServiceGetTranslation = {
  readonly methodName: string;
  readonly service: typeof TranslationService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_translation_pb.GetTranslationRequest;
  readonly responseType: typeof v1_private_translation_pb.GetTranslationResponse;
};

type TranslationServiceCreateTranslation = {
  readonly methodName: string;
  readonly service: typeof TranslationService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_translation_pb.CreateTranslationRequest;
  readonly responseType: typeof v1_private_translation_pb.CreateTranslationResponse;
};

type TranslationServiceCreateOrUpdateTranslation = {
  readonly methodName: string;
  readonly service: typeof TranslationService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_translation_pb.CreateOrUpdateTranslationRequest;
  readonly responseType: typeof v1_private_translation_pb.CreateOrUpdateTranslationResponse;
};

type TranslationServiceDeleteTranslation = {
  readonly methodName: string;
  readonly service: typeof TranslationService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_translation_pb.DeleteTranslationRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type TranslationServiceListTranslations = {
  readonly methodName: string;
  readonly service: typeof TranslationService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_translation_pb.ListTranslationsRequest;
  readonly responseType: typeof v1_private_translation_pb.ListTranslationsResponse;
};

type TranslationServiceDeleteTranslationLocale = {
  readonly methodName: string;
  readonly service: typeof TranslationService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_translation_pb.DeleteTranslationLocaleRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type TranslationServiceImportTranslations = {
  readonly methodName: string;
  readonly service: typeof TranslationService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_translation_pb.ImportTranslationsRequest;
  readonly responseType: typeof v1_private_translation_pb.ImportTranslationsResponse;
};

type TranslationServiceExportTranslations = {
  readonly methodName: string;
  readonly service: typeof TranslationService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_private_translation_pb.ExportTranslationsRequest;
  readonly responseType: typeof v1_private_translation_pb.ExportTranslationsResponse;
};

export class TranslationService {
  static readonly serviceName: string;
  static readonly GetTranslation: TranslationServiceGetTranslation;
  static readonly CreateTranslation: TranslationServiceCreateTranslation;
  static readonly CreateOrUpdateTranslation: TranslationServiceCreateOrUpdateTranslation;
  static readonly DeleteTranslation: TranslationServiceDeleteTranslation;
  static readonly ListTranslations: TranslationServiceListTranslations;
  static readonly DeleteTranslationLocale: TranslationServiceDeleteTranslationLocale;
  static readonly ImportTranslations: TranslationServiceImportTranslations;
  static readonly ExportTranslations: TranslationServiceExportTranslations;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class TranslationServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  getTranslation(
    requestMessage: v1_private_translation_pb.GetTranslationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_private_translation_pb.GetTranslationResponse|null) => void
  ): UnaryResponse;
  getTranslation(
    requestMessage: v1_private_translation_pb.GetTranslationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_private_translation_pb.GetTranslationResponse|null) => void
  ): UnaryResponse;
  createTranslation(
    requestMessage: v1_private_translation_pb.CreateTranslationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_private_translation_pb.CreateTranslationResponse|null) => void
  ): UnaryResponse;
  createTranslation(
    requestMessage: v1_private_translation_pb.CreateTranslationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_private_translation_pb.CreateTranslationResponse|null) => void
  ): UnaryResponse;
  createOrUpdateTranslation(
    requestMessage: v1_private_translation_pb.CreateOrUpdateTranslationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_private_translation_pb.CreateOrUpdateTranslationResponse|null) => void
  ): UnaryResponse;
  createOrUpdateTranslation(
    requestMessage: v1_private_translation_pb.CreateOrUpdateTranslationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_private_translation_pb.CreateOrUpdateTranslationResponse|null) => void
  ): UnaryResponse;
  deleteTranslation(
    requestMessage: v1_private_translation_pb.DeleteTranslationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  deleteTranslation(
    requestMessage: v1_private_translation_pb.DeleteTranslationRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  listTranslations(
    requestMessage: v1_private_translation_pb.ListTranslationsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_private_translation_pb.ListTranslationsResponse|null) => void
  ): UnaryResponse;
  listTranslations(
    requestMessage: v1_private_translation_pb.ListTranslationsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_private_translation_pb.ListTranslationsResponse|null) => void
  ): UnaryResponse;
  deleteTranslationLocale(
    requestMessage: v1_private_translation_pb.DeleteTranslationLocaleRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  deleteTranslationLocale(
    requestMessage: v1_private_translation_pb.DeleteTranslationLocaleRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  importTranslations(
    requestMessage: v1_private_translation_pb.ImportTranslationsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_private_translation_pb.ImportTranslationsResponse|null) => void
  ): UnaryResponse;
  importTranslations(
    requestMessage: v1_private_translation_pb.ImportTranslationsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_private_translation_pb.ImportTranslationsResponse|null) => void
  ): UnaryResponse;
  exportTranslations(
    requestMessage: v1_private_translation_pb.ExportTranslationsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_private_translation_pb.ExportTranslationsResponse|null) => void
  ): UnaryResponse;
  exportTranslations(
    requestMessage: v1_private_translation_pb.ExportTranslationsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_private_translation_pb.ExportTranslationsResponse|null) => void
  ): UnaryResponse;
}

